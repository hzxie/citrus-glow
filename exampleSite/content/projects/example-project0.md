---
title: Example Project 0
type: project
date: 2010-03-16T21:29:00+00:00
featureImg: "/example-highlight0.jpg"
desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
readingTime: 5
categories:
- Category 0
tags:
- Tag 0
- Tag 1
---

##### What is Lorem Ipsum?

**Lorem Ipsum** is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

##### Screenshots

![Scrrenshot 1](/example-highlight0.jpg)
