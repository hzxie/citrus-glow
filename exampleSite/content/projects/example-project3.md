---
title: Example Project 3
type: project
date: 2010-03-16T21:29:00+00:00
featureImg: "/example-highlight3.jpg"
desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
readingTime: 5
categories:
- Category 1
tags:
- Tag 3
- Tag 4
---

##### Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

##### Screenshots

![Scrrenshot 1](/example-highlight3.jpg)
