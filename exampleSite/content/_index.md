---
title: Home
date: 2013-03-28T12:30:00+00:00
---

### Highlight Cards

<div class="row">
  <div class="col-lg-4">
    {{< highlight-card href="/example-project0.md" img="/example-highlight0.jpg" name="Lorem Ipsum" desc="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever." date="Oct. 2022" tag="Tag #1" tagColor="#41b883" >}}
  </div> <!-- .col-lg-4 -->
  <div class="col-lg-4">
    {{< highlight-card href="/example-project1.md" img="/example-highlight1.jpg" name="PageMaker" desc="There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form." date="June. 2021" tag="Tag #2" tagColor="#3572a5" >}}
  </div> <!-- .col-lg-4 -->
  <div class="col-lg-4">
    {{< highlight-card href="/example-project2.md" img="/example-highlight2.jpg" name="Content Here" desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." date="Aug. 2020" tag="Tag #3" tagColor="#b07219" >}}
  </div> <!-- .col-lg-4 -->
</div> <!-- .row -->
