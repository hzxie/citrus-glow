---
author: Hugo Authors
title: Math Typesetting
date: 2019-03-08
desc: A brief guide to setup LaTeX
type: post
featureImg: /example-slide2.jpg
readingTime: 10
latex: true
categories:
- Category 2
tags:
- latex
---

Mathematical notation in a Hugo project can be enabled by using third party JavaScript libraries.
<!--more-->

In this example we will be using [MathJaX](https://mathjax.org/)

**Note:** Use the online reference of [Supported TeX Functions](https://katex.org/docs/supported.html)

### Examples

Inline math: $\varphi = \dfrac{1+\sqrt5}{2}= 1.6180339887…$

Block math:
$$
 \varphi = 1+\frac{1} {1+\frac{1} {1+\frac{1} {1+\cdots} } } 
$$
