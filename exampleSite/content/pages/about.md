---
title: About
url: /about
author: Haozhe Xie
date: 2013-03-28T12:30:00+00:00
head: false
sidebar: false
comments: false
---

## Lorem Ipsum

#### What is Lorem Ipsum?

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

### Where does it come from?

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

#### Highlighted Researches

{{< research-card title="CityDreamer: Compositional Generative Model of Unbounded 3D Cities" authors="Haozhe Xie, Zhaoxi Chen, Fangzhou Hong, Ziwei Liu" pdf="https://arxiv.org/pdf/2309.00610" project="example-project0.md" code="https://github.com/hzxie/City-Dreamer" demo="https://youtu.be/te4zinLTYz0" journal="arXiv 2309.00610" img="/example-highlight0.jpg" >}}

{{< research-card title="Efficient Regional Memory Network for Video Object Segmentation" authors="Haozhe Xie, Hongxun Yao, Shangchen Zhou, Shengping Zhang, Wenxiu Sun" pdf="https://arxiv.org/pdf/2103.12934.pdf" project="example-project1.md" code="https://github.com/hzxie/RMNet" demo="https://youtu.be/ZlohbQbh_ec" conf="CVPR 2021" img="/example-highlight1.jpg" >}}

{{< research-card title="GRNet: Gridding Residual Network for Dense Point Cloud Completion" authors="Haozhe Xie, Hongxun Yao, Shangchen Zhou, Jiageng Mao, Shengping Zhang, Wenxiu Sun" pdf="https://arxiv.org/pdf/2006.03761.pdf" project="example-project2.md" code="https://github.com/hzxie/GRNet" demo="https://youtu.be/doalYIq5jNc" conf="ECCV 2020" img="/example-highlight2.jpg" >}}

{{< research-card title="Pix2Vox: Context-aware 3D Reconstruction from Single and Multi-view Images" authors="Haozhe Xie, Hongxun Yao, Xiaoshuai Sun, Shangchen Zhou, Shengping Zhang" pdf="https://arxiv.org/pdf/1901.11153" project="example-project0.md" code="https://github.com/hzxie/Pix2Vox" demo="https://www.youtube.com/watch?v=2cOwdcqgZ3c" conf="ICCV 2019" img="/example-highlight3.jpg" >}}
