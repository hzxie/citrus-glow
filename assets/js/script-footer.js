/*
 * @File:   script-footer.js
 * @Author: Haozhe Xie
 * @Date:   2024-01-13 00:30:13
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2025-01-09 14:17:35
 * @Email:  root@haozhexie.com
 */
/* String Protorype Extension */
String.prototype.format = function() {
    var newStr = this, i = 0
    while (/%s/.test(newStr)) {
        newStr = newStr.replace("%s", arguments[i++])
    }
    return newStr
}
/* Dark Mode */
$("#dark-mode-switch").on("click", function() {
    let currenetTheme = getCurrentTheme()
    setDarkMode(currenetTheme == "light" ? "dark" : "light")
})
/* Copyright year */
$("#copyright .year").html((new Date()).getFullYear())
/* Full Height Content */
$(window).on("load resize", function() {
    if ($(".full-height.page")) {
        let imgHeight = $("img", ".full-height.page").height(),
            headerHeight = $("#header").outerHeight(true),
            footerHeight = $("#colophon").outerHeight(true) + $("#footer").outerHeight(true),
            remainingHeight = $(window).height() - headerHeight - footerHeight,
            padding = Math.max((remainingHeight - imgHeight) / 2, 0)

        $(".full-height.page").css({
            "padding-top": padding,
            "padding-bottom": padding
        })
    }
})
/* Back to top */
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $("#back-to-top").css("right", "20px")
    } else {
        $("#back-to-top").css("right", "-60px")
    }
})
$("#back-to-top").click(function() {
    $("html, body").animate({scrollTop: 0}, 500)
})
/* Fancybox */
Fancybox.bind("[data-fancybox]", {})
$("[data-fancybox]").each(function() {
    let prev = $(this).prev()
    // The fancybox overwrites existing href
    if (prev.length && prev[0].nodeName == "A" && $(prev).html() == "") {
        $(this).attr("href", $(prev).attr("href"))
        $(this).removeAttr("data-fancybox")
    }
})
/* Gallery */
$(".gallery").each(function() {
    const $gallery = $(this)
    const $items = $gallery.find(".gallery-item")
    let loadedCount = 0

    $gallery.addClass("loading")
    $items.each(function() {
        const $item = $(this)
        const imgUrl = $item.attr("url")
        if (!imgUrl) {
            $gallery.removeClass("loading")
            return
        }
        fetch(imgUrl)
            .then(response => response.blob())
            .then(blob => {
                let imgSrc = URL.createObjectURL(blob);
                $item.html(`
                    <a href="${imgSrc}" data-fancybox="gallery">
                        <img src="${imgSrc}" alt="Gallery Image">
                    </a>
                `)
                ++ loadedCount
                if (loadedCount == $items.length) {
                    $gallery.removeClass("loading")
                }
            })
    })
})
$(".gallery .carousel-control").on("click", function() {
    let thisGallery = $(this).parent()
    if ($(".gallery-inner", thisGallery).hasClass("animate")) {
        return
    }
    let direction = $(this).hasClass("carousel-control-prev") ? "prev" : "next",
        firstItem = $(".gallery-item:first-child", thisGallery),
        lastItem = $(".gallery-item:last-child", thisGallery),
        originItem = direction == "prev" ? lastItem : firstItem,
        insertedItem = originItem.clone().css({marginLeft: "0"}),
        itemHeight = originItem.height(),
        itemWidth = originItem.width(),
        callback = setTimeout(() => {
            originItem.remove()
            $(".gallery-inner", thisGallery).removeClass("animate")
        }, 500)

    $(".gallery-inner", thisGallery).addClass("animate")
    $(".gallery-inner", thisGallery).css("height", itemHeight)
    if (direction == "prev") {
        $(".gallery-inner", thisGallery).prepend(insertedItem.css({marginLeft: -itemWidth}))
        $(insertedItem).animate({marginLeft: "0"}, 250, "swing", callback)
    } else {
        $(".gallery-inner", thisGallery).append(insertedItem)
        firstItem.animate({marginLeft: -itemWidth}, 250, "swing", callback)
    }
})
/* Projects Magical Layouts */
$(window).on("load", function() {
    $("#portfolio").isotope({
        itemSelector : ".col-xl-3",
        layoutMode   : "fitRows"
    })
})
$("#project-selector li").on("click", function() {
    $("#project-selector li").removeClass("active")
    $(this).addClass("active")

    let category = $(this).attr("data-filter")
    if (category) {
        if( category !== "*" ) {
            category = category.replace(category, "." + category)
        }
        $("#portfolio").isotope({filter : category})
    }
})
/* InSite Search */
let index = null,
    isLoadingIndex = false

$("input", ".search.widget").on("keydown focus", function() {
    // Building search index with lunr.js
    if (index == null && !isLoadingIndex) {
        // isLoadingIndex is used to prevent the search index is built in multi-times.
        isLoadingIndex = true
        $.getJSON("/index.json", function(posts) {
            index = new Fuse(posts, {"keys": ["title", "content"]})
            isLoadingIndex = false
            // Manually trigger the event to load the search results
            $("input", ".search.widget").keypress()
        })
    }
    if (isLoadingIndex) {
        return
    }
    $(".list-group", ".search.widget").html("")

    let query = $(this).val().trim()
    if (query) {
        let matches = index.search(query)
        if (matches.length == 0) {
            $(".list-group", ".search.widget").append('<a class="list-group-item list-group-item-action disabled" aria-disabled="true">No matching results.</a>')
        } else {
            for (let i = 0; i < Math.min(matches.length, 10); ++ i) {
                let m = matches[i]["item"]
                $(".list-group", ".search.widget").append('<a href="%s" class="list-group-item list-group-item-action">%s</a>'.format(m["url"], m["title"]))
            }
        }
    }
})
$(".search.widget input, .search.widget .list-group").blur(function() {
    setTimeout(function() {
        if (! $(".search.widget input, .search.widget .list-group-item").is(":focus")) {
            $(".list-group", ".search.widget").html("")
        }
    })
})
