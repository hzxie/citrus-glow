/*
 * @File:   script-header.js
 * @Author: Haozhe Xie
 * @Date:   2024-01-14 20:19:23
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-01-14 20:24:19
 * @Email:  root@haozhexie.com
 */
/* Dark Mode */
function getCurrentTheme() {
    let isSystemDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches,
        currenetTheme = localStorage.getItem("theme")

    return currenetTheme ? currenetTheme : (isSystemDark ? "dark" : "light")
}
function setDarkMode(theme) {
    $("#dark-mode-switch > i.fa").removeClass("fa-sun fa-moon")
    $("html").attr("data-theme", theme)
    localStorage.setItem("theme", theme)
    if (theme == "dark") {
        $("#dark-mode-switch > i.fa").addClass("fa-sun")
    } else {
        $("#dark-mode-switch > i.fa").addClass("fa-moon")
    }
}
