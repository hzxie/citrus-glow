# Citrus Glow

Citrus Glow is a a clean, elegant, but fully functional blog theme for Hugo.
Migrated from a WordPress theme named [Logger](https://github.com/hzxie/Logger). The Hugo version is rewritten with [Bootstrap 5](https://getbootstrap.com/docs/5.3).

[![Counter](https://api.infinitescript.com/badgen/count?name=hzxie/CitrusGlow)](#)
[![Example Site](https://img.shields.io/badge/Example_Site-GitLab_Pages-green)](https://hzxie.gitlab.io/citrus-glow)
[![Demo Site](https://img.shields.io/badge/Demo_Site-Infnite_Script-orange)](https://www.infinitescript.com)

---

![Preview](https://i.imgur.com/1MkSltF.png)

## Features

- Responsive design
- Light, dark, and auto modes
- SEO friendly
- In-site search with `fuse.js`
- [Homepage slideshow](https://hzxie.gitlab.io/citrus-glow)
- [Cover images for posts](https://hzxie.gitlab.io/citrus-glow/blog)
- [Highlight cards](https://hzxie.gitlab.io/citrus-glow/#highlight-cards)
- [Research cards](https://hzxie.gitlab.io/citrus-glow/about#highlighted-researches)
- [Portfolio grid](https://hzxie.gitlab.io/citrus-glow/projects)

## Quick Start

1. **Prerequisites:** Please make sure that you have installed hugo and git.
2. Add the repository to your Hugo project as a submodule.

```
git submodule add https://gitlab.com/hzxie/citrus-glow.git themes/citrus-glow
```

3. I recommend duplicating the [config file](https://gitlab.com/hzxie/citrus-glow/-/blob/master/exampleSite/config.yaml) located in the [example site](https://gitlab.com/hzxie/citrus-glow/-/blob/master/exampleSite) directory. Please be aware that some parameters are mandatory for the theme to function correctly.
4. You can generate your site using `hugo server` and view the generated site at `http://localhost:1313/`.

```
hugo server
```

## Configuration

The example configuration in YAML format can be found in [exampleSite](https://hzxie.gitlab.io/citrus-glow).

## License

The project is open sourced under the MIT license.
